import './App.scss';
import { Route, Routes } from 'react-router-dom';
import Layout from './components/Layout';
import Home from './components/Home';
import Apropos from './components/Apropos';
import Contacte from './components/Contacte';
import Competences from './components/Competences';
import NotFound from './components/NotFound';
import PolitiqueConfidentialite from './components/PolitiqueConfidentialite';
import ReactGA from 'react-ga4';

function App() {

  
  const TRACKING_ID = "G-X1E8FYN20J"; // GOOGLE ANALYTICS ID
  ReactGA.initialize(TRACKING_ID);

  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Home />} />
          <Route path='/apropos' element={<Apropos />} />
          <Route path='/competences' element={<Competences />} />
          <Route path='/contacte' element={<Contacte />} />
          <Route path='/politique-confidentialite' element={<PolitiqueConfidentialite />} />

          <Route path="*" element={<NotFound />} />
        </Route>
      </Routes>
    </div>
  );
}

export default App;
