import { Link } from "react-router-dom";
import AnimatedLetters from "../AnimatedLetters";
import { useEffect, useState } from "react";
import humans1 from "../../assets/images/Humaaans1.png"
import Loader from "react-loaders";

const Home: React.FC = () => {
    const [letterClass, setLetterClass] = useState('text-animate');
  
    const nameArray = [' ' ,'L', 'i', 'l', 'i', 'a', 'n', ' ', 'P', 'a', 'y', 'e', 'u', 'x'];
    const jobArray = [
      'D',
      'é',
      'v',
      'e',
      'l',
      'o',
      'p',
      'p',
      'e',
      'u',
      'r',
      ' ',
      'w',
      'e',
      'b',
      '.',
    ];
  
    useEffect(() => {
      const timeoutId = setTimeout(() => {
        setLetterClass('text-animate-hover');
      }, 4000);
  
      return () => clearTimeout(timeoutId);
    }, []);
  
    return (
      <>
        <div className="container home-page absolute top-40 md:top-1/3 left-0 md:left-40 grid grid-cols-1 md:grid-cols-2 justify-evenly content-evenly justify-items-center items-center">
          <div className="text-zone top-1/3 left-12 md:left-60">
            <h1 className="text-left text-white text-6xl md:text-8xl leading-tight m-0 font-normal font-['Coolvetica']">
              <span className={letterClass}>Bonjour,</span>
              <br />
              <span className={`${letterClass} _14`}>Je suis </span>
              <AnimatedLetters
                letterClass={letterClass}
                strArray={nameArray}
                idx={15}
              />
              <br />
              <AnimatedLetters
                letterClass={letterClass}
                strArray={jobArray}
                idx={20}
              />
            </h1>
            <h2 className="text-gray-400 mt-5 font-normal tracking-widest text-left animate-fadeIn" >
                Développeur Front-End / Angular / Freelance
            </h2>
            <Link to="/contacte" className="animate-fadeIn w-1/3 md:text-xl hover:bg-red-400 hover:text-gray-900 flat-button text-red text-sm font-normal tracking-widest px-2 py-4 border border-red-400 mt-6 float-left whitespace-nowrap">
              CONTACTEZ MOI
            </Link>
          </div>
          <div>
            <img className="animate-fadeIn" src={humans1} alt="" />
          </div>
        </div>
        <Loader active type="ball-zig-zag" />
      </>
    );
  };
  
  export default Home;