import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";
import AnimatedLetters from "../AnimatedLetters";
import { useEffect, useRef, useState } from "react";
import emailjs from '@emailjs/browser';
import './index.scss'
import Loader from "react-loaders";
import anime from 'animejs';

const Contacte: React.FC = () => {
    const [letterClass, setLetterClass] = useState('text-animate');
    const form = useRef<HTMLFormElement | null>(null);
  
    useEffect(() => {
      const timeoutId = setTimeout(() => {
        setLetterClass('text-animate-hover2');
      }, 3000);
  
      return () => clearTimeout(timeoutId);
    }, []);
  
    const sendEmail = (e: React.FormEvent) => {
      e.preventDefault();
  
      if (form.current) {
        emailjs
          .sendForm('service_tjavg7h', 'template_zrypns8', form.current, 'KgGjw6eI0OUH95dqk')
          .then(
            () => {
              alert('Message successfully sent!');
              window.location.reload();
            },
            () => {
              alert('Failed to send the message, please try again');
            }
          );
      }
    };
    useEffect(() => {
      const wave1 =
        "M0 108.306L50 114.323C100 120.34 200 132.374 300 168.476C400 204.578 500 264.749 600 246.698C700 228.647 800 132.374 900 108.306C1000 84.2382 1100 132.374 1150 156.442L1200 180.51V0H1150C1100 0 1000 0 900 0C800 0 700 0 600 0C500 0 400 0 300 0C200 0 100 0 50 0H0V108.306Z";
      const wave2 =
        "M0 250L50 244.048C100 238.095 200 226.19 300 226.19C400 226.19 500 238.095 600 232.143C700 226.19 800 202.381 900 196.429C1000 190.476 1100 202.381 1150 208.333L1200 214.286V0H1150C1100 0 1000 0 900 0C800 0 700 0 600 0C500 0 400 0 300 0C200 0 100 0 50 0H0V250Z";
      const wave3 =
        "M0 250L50 238.095C100 226.19 200 202.381 300 166.667C400 130.952 500 83.3333 600 101.19C700 119.048 800 202.381 900 214.286C1000 226.19 1100 166.667 1150 136.905L1200 107.143V0H1150C1100 0 1000 0 900 0C800 0 700 0 600 0C500 0 400 0 300 0C200 0 100 0 50 0H0V250Z";
      const wave4 =
        "M0 125L50 111.111C100 97.2222 200 69.4444 300 97.2222C400 125 500 208.333 600 236.111C700 263.889 800 236.111 900 229.167C1000 222.222 1100 236.111 1150 243.056L1200 250V0H1150C1100 0 1000 0 900 0C800 0 700 0 600 0C500 0 400 0 300 0C200 0 100 0 50 0H0V125Z";

      anime({
        targets: '.wave-top > path',
        easing: 'linear',
        duration: 7500,
        loop: true,
        d: [
          { value: [wave1, wave2] },
          { value: wave3 },
          { value: wave4 },
          { value: wave1 },
        ],
      });
    }, []);
  
    return (
      <>
        <div className="p-8 contact-page absolute top-40 md:top-[20px] left-0 md:left-80 grid grid-cols-1 md:grid-cols-2 gap-20 justify-evenly content-evenly justify-items-center items-center">
          <div className="text-zone top-1/3 left-12 md:left-60 z-20">
            <h1 className="text-left text-red text-6xl md:text-8xl leading-tight m-0 font-normal font-['Coolvetica'] mb-10 md:mx-4">
              <AnimatedLetters
                letterClass={letterClass}
                strArray={['C', 'o', 'n', 't', 'a', 'c', 't', 'e', ' ', 'm', 'o', 'i']}
                idx={15}
              />
            </h1>
            <p className="animate-fadeIn text-left text-white text-xl md:text-2xl leading-tight m-0 md:m-4 font-extralight">
              Je suis un jeune développeur - disponible pour tous projets. <br />
              N'hésitez pas à me contacter pour toutes questions supplémentaire.
            </p>
            <div className="contact-form w-full mt-5">
              <form ref={form} onSubmit={sendEmail}>
                <ul className="p-0 m-0">
                  <div className="grid grid-cols-1 md:grid-cols-2 gap-0 md:gap-3">
                    <li className="half animate-fadeInUp relative w-full ml-0 md:ml-4 mb-3 text-left clear-none block overflow-hidden ">
                      <input
                        placeholder="Nom"
                        type="text"
                        name="name"
                        required
                        className="w-full bg-[#494670] h-20 text-2xl text-white py-5 px-5 hover:border-b-2 hover:border-red-400"
                      />
                    </li>
                    <li className="half animate-fadeInUp relative w-full ml-0 md:ml-4 mb-3 text-left clear-none">
                      <input
                        placeholder="Email"
                        type="email"
                        name="email"
                        required
                        className="w-full bg-[#494670] h-20 text-2xl text-white py-5 px-5 hover:border-b-2 hover:border-red-400"
                      />
                    </li>
                  </div>
                  
                  <li className="animate-fadeInUp relative w-full ml-0 md:ml-4 mb-3 clear-both">
                    <input
                      placeholder="Sujet"
                      type="text"
                      name="subject"
                      required
                      className="w-full bg-[#494670] h-20 text-2xl text-white py-5 px-5 hover:border-b-2 hover:border-red-400"
                    />
                  </li>
                  <li className="animate-fadeInUp relative w-full ml-0 md:ml-4 mb-3 clear-both">
                    <textarea
                      placeholder="Message"
                      name="message"
                      required
                      className="w-full bg-[#494670] h-48 text-2xl max-w-full text-white p-5 hover:border-b-2 hover:border-red-400"
                    ></textarea>
                  </li>
                  <li>
                    <input
                      type="submit"
                      className="transition duration-300 ease-in animate-fadeInUp hover:bg-[#ff674a] uppercase hover:cursor-pointer hover:text-[#2c2b3d] flat-button text-red border tracking-widest p-5 ml-0 md:ml-4 text-xl w-full md:w-40 border-[#ff674a] float-left text-center"
                      value="Envoyer"
                    />
                  </li>
                </ul>
              </form>
            </div>
          </div>
          <div className="w-full">
            <div className="info-map absolute bg-[#2c2b3dde] top-1/2 md:top-12 right-1/3 z-20 p-5 text-white animate-fadeIn text-left">
              44000 Nantes, France.
              <br />
              <span>Email: payeux.l@gmail.com</span>
            </div>
            <div className="map-wrap bg-[#2c2b3d] w-full md:w-[92vh] h-[92vh] relative z-10">
              
              <MapContainer scrollWheelZoom={false} center={[47.216671, -1.55]} zoom={15} className="h-full">
                <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
                <Marker position={[47.216671, -1.55]}>
                  <Popup>Nous pouvons nous retrouver à Nantes 😉</Popup>
                </Marker>
              </MapContainer>
            </div>
          </div>

          <Loader active type="ball-zig-zag" />
          
        </div>
  
        
      </>
    );
  };
  
  export default Contacte;