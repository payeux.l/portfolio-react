import { faLinkedin, faGithub, faGitlab } from "@fortawesome/free-brands-svg-icons";
import { faHome, faUser, faEnvelope, faGear } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import { Link, useLocation } from "react-router-dom";

const Sidebar = () => {

    const [isSidebarOpen, setIsSidebarOpen] = useState(false);

    const delayedCloseSidebar = () => {
        setTimeout(() => {
          setIsSidebarOpen(false);
        }, 500); // Délai de 500 millisecondes (0.5 secondes) avant de fermer la barre latérale
      };

    const location = useLocation();

  return (
    <div>
      {/* Sidebar */}
      <div
        className={`${
          isSidebarOpen
            ? 'translate-x-0'
            : '-translate-x-full'
        } fixed inset-y-0 z-50 flex w-80 transition-transform duration-300`}
      >
        {/* Curvy shape */}
        <svg
          className="absolute inset-0 w-full h-full text-neutral-950"
          style={{ filter: 'drop-shadow(10px 0 10px #00000030)' }}
          preserveAspectRatio="none"
          viewBox="0 0 309 800"
          fill="currentColor"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M268.487 0H0V800H247.32C207.957 725 207.975 492.294 268.487 367.647C329 243 314.906 53.4314 268.487 0Z"
          />
        </svg>

        {/* Sidebar content */}
        <div className="z-10 flex flex-col flex-1">
          <div className="flex items-center justify-between flex-shrink-0 w-64 p-4">
            {/* Logo */}
            <a href="/" className="text-3xl font-semibold text-red uppercase">
              <span className="sr-only">LP</span>
              Lilian Payeux
            </a>
            {/* Close btn */}
            <button
              onClick={() => setIsSidebarOpen(false)}
              className="p-1 rounded-lg focus:outline-none focus:ring hover:text-red-400"
            >
              <svg
                className="w-8 h-8"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M6 18L18 6M6 6l12 12"
                />
              </svg>
              <span className="sr-only">Close sidebar</span>
            </button>
          </div>
          <nav className="flex flex-col flex-1 w-64 p-4 mt-4 text-2xl ">
            <Link
                to="/"
                onClick={delayedCloseSidebar}
                className={`flex items-center my-8 space-x-2 hover:bg-gray-200 hover:text-gray-700 p-2 rounded-md ${location.pathname === '/' ? 'bg-gray-200 text-gray-700' : ''}`}
            >
                <FontAwesomeIcon icon={faHome} color="#4d4d4e" size="2x" />
                <span className="text-lg text-center font-extrabold">Accueil</span>
            </Link>
            <Link
                to="/apropos"
                onClick={delayedCloseSidebar}
                className={`flex items-center my-8 space-x-2 hover:bg-gray-200 hover:text-gray-700 p-2 rounded-md ${location.pathname === '/apropos' ? 'bg-gray-200 text-gray-700' : ''}`}
            >
                <FontAwesomeIcon icon={faUser} color="#4d4d4e" size="2x" />
                <span className="text-lg text-center font-extrabold">A propos</span>
            </Link>
            <Link
                to="/competences"
                onClick={delayedCloseSidebar}
                className={`flex items-center my-8 space-x-2 hover:bg-gray-200 hover:text-gray-700 p-2 rounded-md ${location.pathname === '/competences' ? 'bg-gray-200 text-gray-700' : ''}`}
            >
                <FontAwesomeIcon icon={faGear} color="#4d4d4e" size="2x" />
                <span className="text-lg text-center font-extrabold">Compétences</span>
            </Link>
            <Link
                to="/contacte"
                onClick={delayedCloseSidebar}
                className={`flex items-center my-8 space-x-2 hover:bg-gray-200 hover:text-gray-700 p-2 rounded-md ${location.pathname === '/contacte' ? 'bg-gray-200 text-gray-700' : ''}`}
            >
                <FontAwesomeIcon icon={faEnvelope} color="#4d4d4e" size="2x" />
                <span className="text-lg text-center font-extrabold">Contacte</span>
            </Link>
          </nav>
          <div className="flex-shrink-0 py-4 text-left pl-4 text-neutral-600">
            <ul className="bottom-20 absolute block p-0 m-0 text-center ">
                <li >
                    <a
                        href="https://www.linkedin.com/in/lilian-payeux"
                        target="_blank"
                        rel="noreferrer"
                    >
                        <FontAwesomeIcon
                        icon={faLinkedin}
                        color="#4d4d4e"
                        className="anchor-icon hover:text-blue-600 text-3xl p-2"
                        />
                    </a>
                </li>
                <li>
                    <a
                        href="https://github.com/LilPay"
                        target="_blank"
                        rel="noreferrer"
                    >
                        <FontAwesomeIcon
                        icon={faGithub}
                        color="#4d4d4e"
                        className="anchor-icon hover:text-slate-200 text-3xl p-2"
                        />
                    </a>
                </li>
                <li>
                    <a href="https://gitlab.com/payeux.l" rel="noreferrer" target="_blank">
                        <FontAwesomeIcon
                        icon={faGitlab}
                        color="#4d4d4e"
                        className="anchor-icon hover:text-orange-600 text-3xl p-2"
                        />
                    </a>
                </li>
                <li>
                    <a
                        href="mailto:payeux.l@gmail.com"
                        rel="noreferrer"
                        target="_blank"
                    >
                        <FontAwesomeIcon
                        icon={faEnvelope}
                        color="#4d4d4e"
                        className="anchor-icon hover:text-red-600 text-3xl p-2"
                        />
                    </a>
                </li>
            </ul>
            <a href="https://www.lilian-payeux.fr" className="text-xl font-LaBelleAurore hover:text-red-400 hover:cursor-pointer">
              Copyright Lilian-payeux.fr
            </a>
          </div>
        </div>
        </div>

        <main className="flex flex-col items-center justify-center flex-1">
        {/* Page content */}
        <button
          onClick={() => setIsSidebarOpen(true)}
          className="fixed p-2 text-white bg-[#0a0a0a] rounded-lg top-5 left-5 z-40"
        >
          <svg
            className="w-12 h-12 md:w-20 md:h-20"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M4 6h16M4 12h16M4 18h16"
            />
          </svg>
          <span className="sr-only">Open menu</span>
        </button>
      </main>
    </div>
   );
}

export default Sidebar;