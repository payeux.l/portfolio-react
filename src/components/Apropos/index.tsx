import { useEffect, useRef, useState } from "react";
import AnimatedLetters from "../AnimatedLetters";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngular, faHtml5, faCss3, faReact, faJsSquare, faJava } from "@fortawesome/free-brands-svg-icons";
import Loader from "react-loaders";
import gsap from "gsap";

const Apropos: React.FC = () => {
  const [letterClass, setLetterClass] = useState('text-animate')

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      setLetterClass('text-animate-hover2');
    }, 3000);

    return () => clearTimeout(timeoutId);
  }, []);

    const iconRefs = useRef<Array<SVGElement | null>>([]);
  
    useEffect(() => {
      gsap.to(iconRefs.current, {
        y: 10,
        duration: 1,
        repeat: -1,
        yoyo: true,
        ease: 'power1.inOut',
      });
    }, []);

  return (
    <>
      <div className="p-8 about-page absolute top-40 md:top-[20%] left-0 md:left-80 right-0 md:right-10 grid grid-cols-1 md:grid-cols-2 justify-evenly content-evenly justify-items-center items-center">
        <div className="text-zone top-1/3 left-12 md:left-60">
          <h1 className="text-left text-red text-6xl md:text-8xl leading-tight m-0 font-normal font-['Coolvetica'] mb-10 mx-4">
            <AnimatedLetters
              letterClass={letterClass}
              strArray={['A', ' ', 'p', 'r', 'o', 'p', 'o', 's']}
              idx={15}
            />
          </h1>
          <p className="animate-fadeIn text-left text-white text-xl md:text-4xl leading-tight font-extralight m-4">
            Je suis un jeune développeur, j'adore le <span className="text-red">Front-End</span> mais je fais également du 
            <span className="text-red"> Back-End</span>. J'ai commencé dès le plus jeune âge avec la création de site web par curiosité avec le HTML/CSS.
          </p>
          <p className="animate-fadeIn text-left text-white text-xl md:text-4xl leading-tight m-4 font-extralight">
            J'ai continué à suivre ma passion en poursuivant des études dans le domaine du développement web. 
            Finalement, j'ai eu la chance de rejoindre une startup en tant que développeur FullStack au sein d'une licence professionnelle en alternance.
            <br />
              Mon parcours a débuté par le <span className="text-red">Front-End</span> avec <span className="text-red">Angular (version 12+)</span>. 
              Ensuite, j'ai plongé dans le monde du <span className="text-red">Back-End</span> en travaillant avec <span className="text-red">Java Spring Boot</span>.
          </p>
          <p className="animate-fadeIn text-left text-white text-xl md:text-4xl leading-tight m-4 font-extralight">
            Pour me décrire brièvement, je suis une personne curieuse et altruiste, constamment en quête de nouvelles connaissances et désireuse de partager mon expertise avec les autres.
          </p>
        </div>

        <div className="stage-cube-cont">
          <div className="cubespinner grid grid-cols-3">
            <div className="face1 p-4 animate-fadeInUp">
              <FontAwesomeIcon icon={faAngular} className="text-[10rem] hover:animate-bounce" color="#DD0031"  ref={(el) => (iconRefs.current[0] = el)}/>
            </div>
            <div className="face2 p-4 animate-fadeInUp">
              <FontAwesomeIcon icon={faHtml5} className="text-[10rem] hover:animate-bounce" color="#F06529" ref={(el) => (iconRefs.current[1] = el)}/>
            </div>
            <div className="face3 p-4 animate-fadeInUp">
              <FontAwesomeIcon icon={faCss3} className="text-[10rem] hover:animate-bounce" color="#28A4D9" ref={(el) => (iconRefs.current[2] = el)}/>
            </div>
            <div className="face4 p-4 animate-fadeInUp">
              <FontAwesomeIcon icon={faReact} className="text-[10rem] hover:animate-bounce" color="#5ED4F4" ref={(el) => (iconRefs.current[3] = el)}/>
            </div>
            <div className="face5 p-4 animate-fadeInUp">
              <FontAwesomeIcon icon={faJsSquare} className="text-[10rem] hover:animate-bounce" color="#EFD81D" ref={(el) => (iconRefs.current[4] = el)}/>
            </div>
            <div className="face6 p-4 animate-fadeInUp">
              <FontAwesomeIcon icon={faJava} className="text-[10rem] hover:animate-bounce" color="#EC4D28" ref={(el) => (iconRefs.current[5] = el)}/>
            </div>
          </div>
        </div>
      </div>
      <Loader active type="ball-zig-zag" />
    </>
  )
}

export default Apropos