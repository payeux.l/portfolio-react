import { Outlet } from "react-router-dom";
import Sidebar from "../Sidebar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDownload } from "@fortawesome/free-solid-svg-icons";

const Layout = () => {
  return (
    <div className='App'>
      <Sidebar />
      <div className='page'>
        <div className="">
          <a 
            target="_blank" 
            rel="noreferrer"
            href="https://drive.google.com/file/d/1phhVU-_LO0uRmj3k4Nq0KTlWINJTsF7K/view?usp=sharing" 
            className="text-xl md:w-1/4 bg-[#ff674a] text-[#2c2b3d] p-4 rounded-b-lg duration-300 font-semibold hover:text-white hover:pt-8 fixed left-1/3 md:left-80 z-40">
            Télécharger mon CV
            <FontAwesomeIcon icon={faDownload} className="px-4"/>
          </a>
        </div>
        <Outlet />
        <span className="tags bottom-tags">
          
          
          <span className="bottom-tag-html"></span>
        </span>
      </div>
    </div>
  )
}

export default Layout;