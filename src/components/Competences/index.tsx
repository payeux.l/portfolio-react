import { useEffect, useState } from "react";
import AnimatedLetters from "../AnimatedLetters";
import Loader from "react-loaders";

const Competences: React.FC = () => {
  const [letterClass, setLetterClass] = useState('text-animate')

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      setLetterClass('text-animate-hover2');
    }, 3000);

    return () => clearTimeout(timeoutId);
  }, []);

  return (
    <>
      <div className="container p-8 about-page absolute top-40 md:top-[20%] left-0 md:left-80 grid grid-cols-1 md:grid-cols-2 justify-evenly content-evenly justify-items-center items-center">
        <div className="text-zone top-1/3 left-12 md:left-60">
          <h1 className="text-left text-red text-6xl md:text-8xl leading-tight m-0 font-normal font-['Coolvetica'] mb-10 mx-4">
            <AnimatedLetters
              letterClass={letterClass}
              strArray={['C', 'o', 'm', 'p', 'é', 't', 'e', 'n', 'c', 'e', 's']}
              idx={15}
            />
          </h1>
          <p className="animate-fadeIn text-left text-white text-xl md:text-4xl leading-tight font-extralight m-4">
            J'ai acquis près de deux années d'expérience professionnelle en tant que développeur Full-Stack. <br />
            Mon parcours a débuté avec une année d'alternance, suivi d'une demi-année en tant que Freelance. <br />
            Mon expertise s'étend principalement à <span className="text-red">Angular 12+</span> et <span className="text-red">Spring Boot</span>, deux technologies que j'ai utilisées pour développer des applications robustes et performantes.
            <br />
            En général je développe en <span className="text-red">React/Angular</span> en <span className="text-red">TypeScript</span>, agrémenté de <span className="text-red">TailwindCss</span>.
          </p>
          <p className="animate-fadeIn text-left text-white text-xl md:text-4xl leading-tight m-4 font-extralight">
            Bien que je ne sois pas un designer de profession, j'attache une grande importance à l'esthétique et à l'expérience utilisateur de mes projets Front-End. 
            J'adopte une approche "mobile first" pour garantir que mes créations sont accessibles sur tous les appareils.
          </p>
          <p className="animate-fadeIn text-left text-white text-xl md:text-4xl leading-tight m-4 font-extralight">
            Pour en savoir plus sur mes projets et mon expérience, je vous invite à consulter mon <a href="/contacte" className="text-red border-b border-red-400">LinkedIn</a> ou à <a href="/contacte" className="text-red border-b border-red-400">télécharger mon CV</a> disponible en haut de la page.          
          </p>
        </div>

        
      </div>
      <Loader active type="ball-zig-zag" />
    </>
  )
}

export default Competences