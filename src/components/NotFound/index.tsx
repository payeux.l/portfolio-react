const NotFound: React.FC = () => {

    return (
        <div>
            <div className="container home-page absolute top-40 md:top-1/3 left-0 md:left-40 grid justify-evenly content-evenly justify-items-center items-center">
                <div className="text-zone top-1/3 left-12 md:left-60">
                    <h1 className="text-left text-white text-6xl md:text-8xl leading-tight m-0 font-normal font-['Coolvetica']">Page not found</h1>
                </div>
            </div>
        </div>
    )

}

export default NotFound